<?php
class PermissionsController extends AuthorizationAppController {
	public $components = array('Authorization.ControllerList');
	
	public function beforeFilter(){
		parent::beforeFilter();
	}
	
	public function index() {
		$plugins = $this->ControllerList->get();
		
		foreach ( $plugins as $plugin => $controllers ){			
			foreach ( $controllers as $controller => $actions ){				
				foreach ( $actions as $index => $action ){
					$permissions = $this->Permission->find('all', array(
						'conditions' => array(
							'Permission.plugin' 	=> $plugin == 'Application' ? null : $plugin,
							'Permission.controller' => $controller,
							'Permission.action' 	=> $action
						)
					));
					
					unset($plugins[$plugin][$controller][$index]);
					$plugins[$plugin][$controller][$action] = array();
					
					foreach ($permissions as $permission){
						$plugins[$plugin][$controller][$action][$permission['Permission']['group_id']] = array(
							'id' 		=> $permission['Permission']['id'],
							'allowed' 	=> $permission['Permission']['allowed'],
						);
					}
				}
			}
		}
		
		$this->set('controllers', $this->ControllerList->controllers());
		$this->set('plugins', $plugins);
        $this->set('groups', $this->Permission->Group->find('list', array('fields' => array('id', 'name'))));
	}
	
	public function update(){
		$this->autoRender = false;
		
		$permissions = array();
		
		foreach ( $this->request->data['Permission']['groups'] as $group_id => $allowed ){
			$permission = array(
				'id'			=> $this->request->data['Permission']['ids'][$group_id],
				'group_id' 		=> $group_id,
				'plugin'		=> !empty($this->request->data['Permission']['plugin'])? $this->request->data['Permission']['plugin'] : null,
				'controller'	=> $this->request->data['Permission']['controller'],
				'action'		=> $this->request->data['Permission']['action'],
				'allowed'		=> $allowed
			);
			
			$this->Permission->save($permission);
			$permission['id'] = $this->Permission->id;
			
			$permissions[] = $permission;
		}
		
		Cache::clear(false, 'authorization');
		
		$this->header('Content-type: application/json');
		echo json_encode($permissions);		
	}
	
	/**
	 *	acctionDenied : default action denied action
	 *	This action only render a view
	 */
	public function actionDenied(){}
    
    /**
	 *	actionDeniedJson : default action denied action
	 *	This action only outputs a json
	 */
	public function actionDeniedJson() {
        $this->autoRender = false;
        
        // Set the response Content-Type to application/json
        $this->response->type('application/json');
        
        echo json_encode(array('success' => false, 'message' => __('You don&rsquo;t have enough permissions to access this url.')));
    }
    
    /**
	 *	notAllowedJson : default action denied action
	 *	This action only outputs a json
	 */
	public function notAllowedJson() {
        $this->autoRender = false;
        
        // Set the response Content-Type to application/json
        $this->response->type('application/json');
        
        echo json_encode(array('success' => false, 'message' => __('Please login to perform this action.')));
    }
}