<?php
App::import('Model', 'Authorization.Permission');

class AuthorizationComponent extends Component {	
	private $Controller 	= null;
	private $Permission 	= null;
	
	/**
	 *	Authentication component
	 */
	private $Authentication = null;
	private $Tracking 		= null;
	
	private $url = null;
	
	private $config = array(
		'authentication' 	=> 'Authentication.Authentication',
		'tracking' 			=> 'Authorization.Tracking',
		'adminsGroups'			=> array(),
		'trackingFree'		=> array(
			#'plugin#controller#action',
			'authorization#permissions#actiondenied',
            'authorization#permissions#actiondeniedjson',
            'authorization#permissions#notallowedjson',
			'#users#register',
			'#users#login'
		),
		'permissionFree'	=> array(
			#'plugin#controller#action',
			'authorization#permissions#actiondenied',
            'authorization#permissions#actiondeniedjson',
            'authorization#permissions#notallowedjson',
			'authentication#users#register',
			'authentication#users#login'
		),
		'actionDenied' 		=> array('plugin' => 'authorization', 'controller' => 'permissions', 'action' => 'actionDenied'),
        'actionDeniedJson' 	=> array('plugin' => 'authorization', 'controller' => 'permissions', 'action' => 'actionDeniedJson'),
		'notAllowed' 		=> array('plugin' => 'authentication', 'controller' => 'users', 'action' => 'login'),
        'notAllowedJson' 	=> array('plugin' => 'authorization', 'controller' => 'permissions', 'action' => 'notAllowedJson')
	);
	
	public function __construct(ComponentCollection $collection, $settings = array()) {
		parent::__construct($collection, $settings);
		
		# merge config with settings give when component is called
		$this->config = array_merge($this->config, $settings);
		
		# import Authentication component
		if ( App::import('Component', $this->config['authentication']) ){
			$authenticationComponent = substr($this->config['authentication'], strpos($this->config['authentication'], '.') + 1, strlen($this->config['authentication'])) . 'Component';
			
			$this->Authentication = new $authenticationComponent($collection, $settings);	
		} else {
			throw new NotFoundException('Could not find ' . $this->config['authentication'] . ' component');	
		}
		
		# if Tracking is enabled
		if ( $this->config['tracking'] ){
			# import Tracking model
			if ( App::import('Model', $this->config['tracking']) ){
				$trackingModel = substr($this->config['tracking'], strpos($this->config['tracking'], '.') + 1, strlen($this->config['tracking']));
				
				$this->Tracking = new $trackingModel;
			} else {
				throw new NotFoundException('Could not find ' . $this->config['tracking'] . ' model');	
			}
		}

		$this->Permission = new Permission();
	}
	
	public function initialize(Controller $controller) {		
		$this->Controller =& $controller;
		
		$this->url = $this->url = strtolower(implode('#', array(
			$this->Controller->request->params['plugin'],
			$this->Controller->request->params['controller'],
			$this->Controller->request->params['action']
		)));
	}

	public function startup(Controller $controller) {		
		if ( !$this->Authentication->User->is('logged') && !$this->Authentication->User->is('guest') ) {
			$this->Authentication->login('guest');
		}
		
		$authorized = $this->__authorize();
	}
	
	public function beforeRender(Controller $controller) {
		$controller->set('user', $this->Authentication->User);
	}
	
	public function shutdown(Controller $controller) {	
	}
	
	public function beforeRedirect(Controller $controller) {
	}
	
	private function __allowed() {
		return in_array($this->url, $this->config['permissionFree']) || in_array($this->url, $this->Permission->group($this->Authentication->User->Group->id()));
	}
	
	private function __authorize() {
		if ( in_array($this->Authentication->User->Group->id(), $this->config['adminsGroups']) || in_array($this->Authentication->User->Group->name(), $this->config['adminsGroups'] ) ) {
			return true;
		}
		
		$allowed = $this->__allowed();
		
		// Track request only if Tracking model exist
		if ( $this->Tracking ) {
			$this->__track($allowed);
		}
		
		if ( !$allowed ){
			if ( $this->Authentication->User->is('logged') ) {
                if ( $this->Controller->request->is('ajax') ) {
                    $this->Controller->redirect($this->config['actionDeniedJson']);
                }
				
                $this->Controller->redirect($this->config['actionDenied']);
			} else {
                if ( $this->Controller->request->is('ajax') ) {
                    $this->Controller->redirect($this->config['notAllowedJson']);
                }
				
                $this->Controller->Session->setFlash(__('No enaugh permission.'));
                $this->Controller->redirect($this->config['notAllowed']);	
			}
		}
	}
	
	private function __track( $allowed = true ) {
		$track = false;
		
		if ( !in_array($this->url, $this->config['trackingFree']) ){
			$track = $this->Tracking->track($this->Authentication->User, $this->Controller->request->params, $allowed);	
		}
		
		return $track;	
	}
}