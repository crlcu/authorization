<?php
class ControllerListComponent extends Component {
	public function __construct(ComponentCollection $collection, $settings = array()) {
		parent::__construct($collection, $settings);
		
		$this->controllerMethods = get_class_methods('Controller');
	}
	
	/**
	 *  Used to get controller's list
	 *
	 * @access public
	 * @return array
	 */
	public function controllers() {				
		$controllers = App::objects('Controller');
		
		// remove AppController from list
		if (($key = array_search('AppController', $controllers)) !== false) {
			unset($controllers[$key]);
		}
        
		foreach ($controllers as $key => $controller) {			
            $controllers['Application'][] = str_replace('Controller', '', $controller);
			unset($controllers[$key]);
		}
		
		$plugins = App::objects('plugins');
		
		foreach ($plugins as $plugin) {
			$pluginControllerClasses = App::objects($plugin.'.Controller');
			
			foreach ($pluginControllerClasses as $controller) {
                if ( strpos($controller, 'AppController' ) === false )
                    $controllers[$plugin][] = str_replace('Controller', '', $controller);
			}
		}
		
		$controllers[-1] = __('All');
		
		//sort controllers by key
		ksort ($controllers);
        
		return $controllers;
	}
	
	/**
	 * Used to get all controllers with all methods for permissions
	 *
	 * @access public
	 * @return array
	 */
	public function get() {
		$controllerClasses = App::objects('Controller');
		
		$controllers = array();
		
		$plugins = $this->controllers();
		unset($plugins[-1]);
		
		foreach ( $plugins as $_plugin => $_controllers ) {
			foreach ( $_controllers as $controller ) {
				$actions = $this->__getControllerMethods($_plugin, $controller);
				
				$controllers[$_plugin][$controller] = $actions;
			}
		}
		
		return $controllers;
	}
	
	/**
	 * Used to get methods of controller
	 *
	 * @access private
	 * @param string $controllername Controller name
	 * @param array $superParentActions Controller class methods
	 * @param array $parentActions App Controller class methods
	 * @param string $p plugin name
	 * @return array
	 */
	private function __getControllerMethods($plugin, $controller) {
		$object = str_replace('Application.', '', $plugin . '.'. $controller);
		
		App::import('Controller', $object);
		
		$actions = get_class_methods($controller.'Controller');
		
		if ( !empty($actions) ) {
			$actions = $this->__removePrivateActions($actions);
			$actions = array_diff($actions, $this->controllerMethods);
		}
		
		return $actions;
	}
	
	/**
	 * Used to delete private actions from list of controller's methods
	 *
	 * @access private
	 * @param array $actions Controller's action
	 * @return array
	 */
	private function __removePrivateActions($actions) {
		foreach ($actions as $index => $action) {
			if ($action{0} == '_') {
				unset($actions[$index]);
			}
		}
		
		sort($actions);
	
		return $actions;
	}
}