# Authorization Plugin
* Author:  Adrian Crisan (crycul2oo4@yahoo.com.com)

## Requirements

The master branch has the following requirements:

* CakePHP 2.2.0 or greater.
* PHP 5.3.0 or greater.

## Installation

* Clone/Copy the files in this directory into `app/Plugin/Authorization`
		
		git clone git://github.com/crlcu/CakePHP-Authorization-Plugin.git app/Plugin/Authorization
		
* Ensure the plugin is loaded in `app/Config/bootstrap.php` by calling `CakePlugin::load('Authorization');`

		//app/Config/bootstrap.php
		CakePlugin::load('Authorization');
		
* Include the Authorization component in your `AppController.php`:


		public $components = array('Authorization.Authorization');
   
### Once installed
* Run schema create command

		Console\cake schema create -p Authorization
		
## Configuration

The Authorization has a few configuration settings. Settings are passed in the component declaration like normal component configuration.

	public $components = array(
		'Authorization.Authorization' => array(/* array of settings */)
	);


### adminsGroups


		public $components = array(
			'Authorization.Authorization' => array(
				'adminsGroups' => array(1, 2, 3)
			)
		);
		
or
		
		public $components = array(
			'Authorization.Authorization' => array(
				'adminsGroups' => array('admin', 'master', 'other-admin-group-name')
			)
		);

### tracking
The `tracking` setting is an string containing your tracking model.

This model must have `track` function which get ($this->Authentication->User, $this->Controller->request->params, $allowed) arguments


		public $components = array(
			'Authorization.Authorization' => array(
				'tracking' => 'Your.TrackingModel',
			)
		);
	
### trackingFree
The `trackingFree` setting is an array of actions that doesn't need to be tracked when tracking is on


		public $components = array(
			'Authorization.Authorization' => array(
				'trackingFree' => array(
					#'plugin#controller#action',
					'authorization#permissions#actionDenied',
					'#users#register',
					'#users#login'
				)
			)
		);
		
### permissionFree
The `permissionFree` setting is an array of actions that doesn't need to check for permission


		public $components = array(
			'Authorization.Authorization' => array(
				'permissionFree' => array(
					#'plugin#controller#action',
					'authorization#permissions#actionDenied',
					'#users#register',
					'#users#login'
				)
			)
		);
		
### actionDenied
The `actionDenied` setting is an action/url to redirect a logged in user when doesn't have permission to access a specific action


		public $components = array(
			'Authorization.Authorization' => array(
				'actionDenied' => array('plugin' => 'authorization', 'controller' => 'permissions', 'action' => 'actionDenied')
			)
		);
		
### notAllowed
The `notAllowed` setting is an action/url to redirect a guest user when doesn't have permission to access a specific action


		public $components = array(
			'Authorization.Authorization' => array(
				'notAllowed' => array('plugin' => 'authorization', 'controller' => 'permissions', 'action' => 'register')
			)
		);