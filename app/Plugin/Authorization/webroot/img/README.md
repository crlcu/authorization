# Authorization Plugin
* Author:  Adrian Crisan (crycul2oo4@yahoo.com.com)

# Install and Setup

* First clone the repository into your `app/Plugin/Authorization` directory
	git clone git://github.com/crlcu/CakePHP-Authorization-Plugin.git app/Plugin/Authorization

* Load the plugin in your `app/Config/bootstrap.php` file:
	//app/Config/bootstrap.php
	CakePlugin::load('Authorization');