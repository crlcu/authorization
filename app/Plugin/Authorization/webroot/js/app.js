$(document).ready(function(){
	$('form.groups').on('click', 'input[type="checkbox"]', function(){
		var $form = $(this).closest('form');
		
		var jqxhr = $.ajax({
			type: 		$form.attr('method'),
			url: 		$form.attr('action'),
			data: 		$form.serialize(),
			dataType:	'json',
			beforeSend: function ( xhr ) {
				// if this is first time when a permission is set for this action
				// disable checkboxes until the permissions ids are retrieved from server
				if ( !$('input.id:first', $form).val() ){
					$('input[type="checkbox"]', $form).attr('disabled', true);
				}
				
				$.ambiance({
					width: 		100,
					message:	'Updating!',
					timeout:	1
				});
			}
		});
		
		jqxhr.fail(function( jqXHR, textStatus, errorThrown ) {
			var error;
			var reason;
			
			if ( textStatus == 'error' ){
				// Error 404 : Not Found
				error = 'Error ' + jqXHR.status;
				reason = 'Update URL ' + jqXHR.statusText;
				
				// Error 500 : Internal Server Error
				if ( jqXHR.status == 500 )
					reason = jqXHR.statusText;
			} else {
				error = 'Unexpected error';
				reason = 'Response is not json or you don&rsquo; have access to update permissions.';
			}
			
			$.ambiance({
				title:	 	error,
				message:	reason,
				type:		'error',
				timeout:	10
			});
		});
		
		jqxhr.done(function ( data ) {
			// if this is first time when a permission is set for this action
			// set the ids for permissions, then enable checkboxes
			if ( !$('input.id:first', $form).val() ) {
				$.each(data, function(index, permission) {
					$('input.id[for="' + permission.group_id + '"]', $form).val(permission.id);
				});
				
				$('input[type="checkbox"]', $form).attr('disabled', false);
			}
			
			$.ambiance({
				width: 		100,
				message:	'Success!',
				type:		'success',
				timeout:	1
			});
		});
	});
});