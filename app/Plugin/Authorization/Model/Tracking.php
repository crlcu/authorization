<?php
class Tracking extends AuthorizationAppModel {
	
	public function track($user, $request, $allowed){
		$track = array(
			'user_id' 		=> $user->id(),
			'user_agent'	=> $user->agent(),
			'request' 		=> json_encode($request),
			'allowed'		=> (int)$allowed
		);
		
		return $this->save($track);
	}
}