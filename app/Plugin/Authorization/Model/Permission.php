<?php
class Permission extends AuthorizationAppModel {
    public $belongsTo = array(
        'Group' => array(
            'className' => 'Authentication.Group',
            'dependent' => true
        )
    );
    
	public function group( $group_id ) {		
		if ( !$allowed = Cache::read('group_' . $group_id, 'authorization') ){
			$permissions = $this->find('all', array(
				'conditions' => array(
					$this->name . '.group_id' 	=> $group_id,
					$this->name . '.allowed' 	=> true
				)
			));
			
			foreach ($permissions as $permission ){
				$allowed[] = strtolower(implode('#', array(
                    $permission['Permission']['plugin'],
                    $permission['Permission']['controller'],
                    $permission['Permission']['action']
                )));
			}
			
			Cache::write('group_' . $group_id, $allowed, 'authorization');
		}
		
		return (array)$allowed;
	}
}