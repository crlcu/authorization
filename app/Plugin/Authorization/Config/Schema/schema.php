<?php
App::import('Model', 'Authorization.Permission');

class AuthorizationSchema extends CakeSchema {

	public function before($event = array()) {
		return true;
	}

	public function after($event = array()) {
	}
	
	public function __destruct() {
		$permission = new Permission();
		
		$permissions = array(
			array(
				'group_id' => '1',
				'plugin' => 'Authorization',
				'controller' => 'Permissions',
				'action' => 'actionDenied',
				'allowed' => '1'
			),
			array(
				'group_id' => '2',
				'plugin' => 'Authorization',
				'controller' => 'Permissions',
				'action' => 'actionDenied',
				'allowed' => '1'
			),
			array(
				'group_id' => '3',
				'plugin' => 'Authorization',
				'controller' => 'Permissions',
				'action' => 'actionDenied',
				'allowed' => '1'
			),
			array(
				'group_id' => '1',
				'plugin' => 'Authorization',
				'controller' => 'Permissions',
				'action' => 'index',
				'allowed' => '1'
			),
			array(
				'group_id' => '2',
				'plugin' => 'Authorization',
				'controller' => 'Permissions',
				'action' => 'index',
				'allowed' => '0'
			),
			array(
				'group_id' => '3',
				'plugin' => 'Authorization',
				'controller' => 'Permissions',
				'action' => 'index',
				'allowed' => '0'
			),
			array(
				'group_id' => '1',
				'plugin' => 'Authorization',
				'controller' => 'Permissions',
				'action' => 'update',
				'allowed' => '1'
			),
			array(
				'group_id' => '2',
				'plugin' => 'Authorization',
				'controller' => 'Permissions',
				'action' => 'update',
				'allowed' => '0'
			),
			array(
				'group_id' => '3',
				'plugin' => 'Authorization',
				'controller' => 'Permissions',
				'action' => 'update',
				'allowed' => '0'
			)
		);
		
		$permission->saveAll($permissions);
	}

	public $authorization_permissions = array(
		'id' => array('type' => 'biginteger', 'null' => false, 'default' => null, 'key' => 'primary'),
		'group_id' => array('type' => 'biginteger', 'null' => false, 'default' => null),
		'plugin' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 50, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'controller' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 50, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'action' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 50, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'allowed' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 4),
		'lastupdate' => array('type' => 'timestamp', 'null' => false, 'default' => 'CURRENT_TIMESTAMP'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

	public $authorization_trackings = array(
		'id' => array('type' => 'biginteger', 'null' => false, 'default' => null, 'key' => 'primary'),
		'user_id' => array('type' => 'biginteger', 'null' => true, 'default' => null),
		'user_agent' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 100, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'request' => array('type' => 'text', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'allowed' => array('type' => 'integer', 'null' => true, 'default' => null, 'length' => 4),
		'lastupdate' => array('type' => 'timestamp', 'null' => false, 'default' => 'CURRENT_TIMESTAMP'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

}
