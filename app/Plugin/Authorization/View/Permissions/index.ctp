<?=$this->Form->input('controllers', array('label' => false, 'options' => $controllers));?>

<?php
	$groups = array(
		1 => 'admin',
		2 => 'users',
		3 => 'guest'
	);
?>

<table class="table">
	<thead>
		<tr>
			<th><?=__('Plugin')?></th>
			<th><?=__('Controller')?></th>
			<th><?=__('Action')?></th>
			<th><?=__('Groups')?></th>
		</tr>
	</thead>
	
	<tbody>
		<?php foreach ( $plugins as $plugin => $controllers):?>
			<?php foreach ( $controllers as $controller => $actions ):?>
				<?php foreach ( $actions as $action => $allowed ):?>
					<tr>
						<td><?=$plugin?></td>
						<td><?=$controller?></td>
						<td><?=$action?></td>
						<td>
							<?=$this->Form->create('Permission', array('action' => 'update', 'class' => 'groups'))?>
								<?php
									echo $this->Form->input('plugin', array('type' => 'hidden', 'value' => $plugin == 'Application' ? null : $plugin));
									echo $this->Form->input('controller', array('type' => 'hidden', 'value' => $controller));
									echo $this->Form->input('action', array('type' => 'hidden', 'value' => $action));
								?>
								
								<?php foreach ( $groups as $id => $name ):?>
									<?=$this->Form->input('Permission.ids.' . $id, array('type' => 'hidden', 'value' => isset($allowed[$id])? $allowed[$id]['id'] : false, 'class' => 'id', 'for' => $id ))?>
									<?=$this->Form->input('Permission.groups.' . $id, array('type' => 'checkbox', 'label' => $name, 'id' => $plugin.$controller.$action.$id, 'checked' => isset($allowed[$id])? $allowed[$id]['allowed'] : false))?>
								<?php endforeach?>
							<?=$this->Form->end();?>
						</td>
					</tr>
				<?php endforeach?>
			<?php endforeach?>
		<?php endforeach?>
	</tbody>
</table>
	