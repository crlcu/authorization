--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `lastupdate`) VALUES
(1, 'admin', '2013-03-14 16:19:42'),
(2, 'user', '2013-03-14 16:19:42'),
(3, 'guest', '2013-03-15 07:20:11');

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `group_id`, `username`, `password`, `salt`, `created`, `modified`) VALUES
(1, 1, 'admin', '9e831d2647213d29705c517f0c5745ec', 'df80129a22e9511270021cf2f0c797e2', NULL, NULL),
(2, 2, 'user', '8b50b84adeab632c9c4db8d07fb03142', '7562b82ceb8419ba3b6843c074cf71a3', NULL, NULL),
(3, 3, 'guest', '', '', NULL, NULL);