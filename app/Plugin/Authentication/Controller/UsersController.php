<?php
class UsersController extends AuthenticationAppController {
	public $components = array('Session');
	
	public function register(){
		
	}
	
	public function login() {
		if ($this->request->is('post')) {			
			if ( $user = $this->User->login( $this->request->data['User'] ) ) {
				$this->Authentication->login($user);
			} else {
				//$this->Session->setFlash(__('Invalid username or password, try again'));
			}
		}
	}
	
	public function logout() {
		$this->Authentication->logout($user);
	}
}