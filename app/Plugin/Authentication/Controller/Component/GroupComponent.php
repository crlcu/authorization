<?php
define('GUEST_GROUP_ID', 3);

class GroupComponent extends Component {
	public $components = array('Session');
	
	private $sessionKey = 'Authentication.Group';
	
	public function __construct(ComponentCollection $collection = null, $settings = array()) {
		parent::__construct($collection, $settings);
	}
	
	public function initialize(Controller $controller) {
		//debug('initialize');
	}

	public function startup(Controller $controller) {
		//debug('startup');
	}
	
	public function __call($method, $params) {		
        return $this->Session->read( $this->sessionKey . '.' . $method );
    }
	
	public function id() {		
		$id = $this->Session->read( $this->sessionKey . '.');
		return !empty($id)? $id : GUEST_GROUP_ID;
	}
	
	public function is( $group ){
		return $this->Session->read( $this->sessionKey . '.name') == $group;
	}
}