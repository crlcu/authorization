<?php
define('GUEST_USER_ID', 3);

App::import('Model', 'Authentication.User');

class UserComponent extends Component {
	public $components = array('Session', 'Authentication.Group');
	
	private $Controller 	= null;
	private $User 			= null;
	
	private $sessionKey		= 'Authentication.User';
	
	public function __construct(ComponentCollection $collection = null, $settings = array()) {
		parent::__construct($collection, $settings);
	}
	
	public function initialize(Controller $controller) {
		//debug('initialize');
	}

	public function startup(Controller $controller) {
		//debug('startup');
	}
	
	public function __call($method, $params) {
        return $this->Session->read( $this->sessionKey . '.' . $method );
    }
	
	public function agent() {
		return $this->Session->read('Config.userAgent');
	}
	
	public function guest() {
		if ( !$guest = Cache::read('user_' . GUEST_USER_ID, 'authentication') ) {
			$this->User = new User();
			$guest = $this->User->findById( GUEST_USER_ID );
			
			Cache::write('user_' . GUEST_USER_ID, $guest, 'authentication');
		}
		
		return $guest;
	}
	
	public function is( $type ) {
		switch ( $type ) {
			case 'logged':
				return $this->isLogged();
				break;
			default:
				return $this->Group->is( $type );
				break;
		}	
	}
	
	public function isLogged() {
		return $this->id() && $this->id() != GUEST_USER_ID ? true : false;
	}
}