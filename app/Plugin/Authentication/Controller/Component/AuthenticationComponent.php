<?php
class AuthenticationComponent extends Component {
	public $components = array('Session', 'Authentication.User');
	
	private $Controller 	= null;
	private $sessionKey		= 'Authentication';
	
	private $config = array(
		'loginRedirect' => array('plugin' => 'authorization', 'controller' => 'permissions', 'action' => 'index'),
		'logoutRedirect' => array('plugin' => 'authentication', 'controller' => 'users', 'action' => 'login')
	);
	
	public function __construct(ComponentCollection $collection = null, $settings = array()) {
		parent::__construct($collection, $settings);
		
		# merge config with settings give when component is called
		$this->config = array_merge($this->config, $settings);
	}
	
	public function initialize(Controller $controller) {
		//debug('initialize');
		$this->Controller =& $controller;
	}

	public function startup(Controller $controller) {
		//debug('startup');
	}
	
	public function login( $user ) {
		$isGuest = $user == 'guest';
		
		if ( $isGuest ) {
			$user = $this->User->guest();
		}
		
		$this->Session->write($this->sessionKey, $user);
		
		if ( !$isGuest ) {
			$this->Controller->redirect($this->config['loginRedirect']);	
		}
	}
	
	public function logout() {
		$this->Session->delete($this->sessionKey);
		$this->Controller->redirect($this->config['logoutRedirect']);
	}
}