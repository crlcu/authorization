<?php
class User extends AuthenticationAppModel {
	public $belongsTo = array(
		'Group' => array(
			'className' => 'Authentication.Group'
		)
	);
	
    /*public $validate = array(
		'username' => array(
			'notEmpty' => array(
                'rule'		=> 'notEmpty',
                'required'	=> true,
                'message' 	=> 'Please enter username'
            ),
			'isUnique' => array(
                'rule' 		=> 'isUnique',
                'required'	=> true,
				'message'	=> 'Username already exist'
            )
        ),
        'password' => array(
            'required' => array(
                'rule' => array('notEmpty'),
                'message' => 'A password is required'
            )
        ),
        'role' => array(
            'valid' => array(
                'rule' => array('inList', array('admin', 'author')),
                'message' => 'Please enter a valid role',
                'allowEmpty' => false
            )
        )
    );*/
	
	public function login ( $credentials ) {
		$user = $this->findByUsername( $credentials['username'] );
		
		if ( !$user ) {
			SessionComponent::setFlash(__('This user does not exist in our database'));
			return false;
		}
		
		if ( $user['User']['password'] === $this->encrypt($credentials['password'], $user['User']['salt']) ) {
			return $user;
		}
		
		return false;
	}
	
	/**
	 * Used to make salt in hash format
	 *
	 * @access public
	 * @return hash
	 */
	public function salt() {		
		return md5( mt_rand(0, 32) . time() );
	}
	
	/**
	 * Used to make password in hash format
	 *
	 * @access public
	 * @param string $password password of user
	 * @param string $salt salt of user for password
	 * @return hash
	 */
	public function encrypt($password, $salt) {
		return md5( md5($password) . md5($salt) );
	}
}